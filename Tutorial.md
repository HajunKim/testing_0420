Tutorial
==========



---------------------------------------




# Intro



### This tutorial explains how to upload files to the remote repository.




---------------------------------------




# Contents



## How to upload files to Bitbucket in Windows
	
	
	
#### 1) Introduction
		
		
#### 2) Install Git and SourceTree
		
		
#### 3) Create a new folder
		
		
#### 4) Create a new repository

		
#### 5) Create a new project to upload
		
		
#### 6) How to go back to before?
	
		
#### 7) Create a branch
		
		
#### 8) Merge one branch to another branch
		
		
#### 9) Upload the project from a local repository to a remote repository
		
		
		

---------------------------------------




## How to upload files to Bitbucket in Windows



***



### 1) Introduction
		
		
		
#### This section explains how to upload files in Windows.
		
		



***



### 2) Install Git and SourceTree



#### Here is the link :


#### [Git](https://git-scm.com/)
		
		
#### [SourceTree](https://www.sourcetreeapp.com/)
		
		
#### Please, download the above programs.



***



### 3) Create a new folder

#### you have to create a folder to connect with Git.
		
#### To give you an example, I made 'gitgit' folder at C:￦gitgit and a html file, named 'index.html'.



***



### 4) Create a new repository
		
		
		
#### Open the SourceTree.














![img1](./image/1.jpg#center)























***



#### Now, we are going to create a local repository that will be tracked by Git in SourceTree.


![img2](./image/2.jpg)


#### Once you do as the above, you can see the local repository that contains the staging area and working directory.
		
#### The working directory, staging area and local repository belong to local section such as your computer.
		
#### Something changed is shown when a project is tracked. 

#### The projects in the staging area is tracked by Git, and projects in working directory is not tracked by Git. 
		
#### For now, index.html is not tracked yet.

#### In other words, Git didn't record the changes of the file.
		
		
		
***
		
		
		
### 5) Commit a project
		
####  Now, we are going to commit the file.
		
#### If GUI requires your account, please type in your account such as the Bitbucket account.
		
		
![img3](./image/3.jpg)
		


***


		
#### If following the above process, you could see the following.
		
		
![img4](./image/4.jpg)


#### 'Commit' is done.



***



#### From now on, Git is recording the history of modifications of the file.
		
	
	
	
![img](./image/5.jpg)




#### To figure out the recording, I added one line into the file and saved as the above.



***


		
![img](./image/6.jpg)
		
#### Then, uncommitted changes are shown in the workspace.



***



	
![img](./image/7.jpg)

#### You can check what are changed as well.
	


***



![img](./image/8.jpg)
		



#### Now, I will commit the changed file.



***




![img](./image/9.jpg)




#### Then, The master branch is changed, including the modifications.
		


***
		
### 6) How to go back to before? 



#### Sometimes, we might commit unwanted files.

#### I would like to introduce 'Reset' method.

#### Let's see one example.
	
	
	
***
	
	
	
		
![img](./image/12.jpg)
		
#### Let's say that a file contains unwanted changes as the above.



***



		
![img](./image/13.jpg)
		
		
		
		
#### Unfortunately, I already committed the file. 

#### How could I go back to before?



***



		
![img](./image/14.jpg)




#### You can just choose the lastest version which you want to go back to

#### And, click 'reset current branch to this commit'



***



![img](./image/15.jpg)
		
#### There are 3 ways to reset to commit.

#### 'Soft' means to keep all local changes

#### 'Mixed' means to keep working copy but reset the staging area

#### 'Hard' means to discard all working copy changes
		
		
		
***



![img](./image/16.jpg)
	
	
#### After 'reset method', uncommitted changes are shown.
	

***		



### 7) Create a branch



#### You can use 'branch' when you want to make experimental codes which are separated from original codes.

#### Experimental codes could include uncertainties or new functions compared to original codes.
		

![img](./image/17.jpg)



#### To create a branch, click a branch button.



***


		
![img](./image/18.jpg)



#### Next, type in the branch name.



***


		
![img](./image/19.jpg)

#### New branch is shown!



***		



### 8) Merge one branch to another branch



#### If you finish making an experimental code, you could want to merge it into the original code.

#### Then, you could use 'merge'
		
		
![img](./image/20.jpg)


#### Choose the branch that you want to merge into the original code(master)

#### Then, click 'Merge (File_name) into current branch'.
	
	
	
***



### 9) Upload the project from a local repository to a remote repository



#### You can send the projects in the local repository to the remote repository.



#### I would like to introduce two cases.

#### In the first case, I would let you know how to upload the projects to the remote repository such as my own Bitbucket repository.

#### In the second case, I would let you know how to collaborate with others by using 'pull'
	
	
	
***		
		
		
#### First, let's see the 1st case.
		
![img](./image/21.jpg)
		
#### Click the 'repository' button as the above

#### And then click the 'add a remote repository' button.
	
	
	
***



![img](./image/27.JPG)
		
#### Click the add button
		
		
		
***



![img](./image/28.JPG)

#### Next, copy and paste the address of the remote repository.



***



![img](./image/29.JPG)

#### The address of the remote repository is shown.



***



#### Now, it is time to upload file.
		
		
![img](./image/22.jpg)
		
		
#### Click the push button



***



![img](./image/23.jpg)
		

#### Choose branches you want to upload to the remote repository.



***



![img](./image/24.jpg)


#### Then, you could find out that the file is uploaded to the remote repository.
	
	
		
***	



#### Next, let's see the second case.

#### In this case, I would like to explain how to collaborate with others.
		
![img](./image/25.jpg)

#### Click the 'Clone' button 



***



![img](./image/26.jpg)
	
	
#### First, type in the remote repository address of others.

#### And, type in the location(address) where you download files in your computer.
	
	
	
	

#### In most cases, we collaborate with others in the one remote repository.

#### Before you start working, you have to implement 'fetch' and then 'pull'

#### 'Fetch' enables you to see whether others add sources or not.

#### 'Pull' enables you to download the files of others on your local repository.
 
	
	

--------------


##### If you want some modifications or something new to add in this tutorial, please feel free to contact me, hajun0219@kaist.ac.kr

##### This tutorial is written by Hajun Kim, a member of humanoid robot research center, KAIST
